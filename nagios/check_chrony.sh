#!/bin/bash

#
# Nagios check for chrony (NTP)
#
# Check the chrony service status and ensure LEAP status is normal.
# If LEAP status not normal, test connectivity between local machine and NTP server.
# If LEAP status is normal, compare local time to NTP server time.
#
# 18/01/2018	- Paul Wetering	- Initial Version
# 19/01/2018	- Paul Wetering	- Some bugs fixed and sanitized source
#

if [[ -z $1 ]] || [[ -z $2 ]]
then
	echo 'Usage: check_chrony.sh <warning_seconds> <critical_seconds>'
	exit 2
fi

INTEGER_VALIDATION='^[0-9]+$'
if ! [[ $1 =~ $INTEGER_VALIDATION ]] || ! [[ $2 =~ $INTEGER_VALIDATION ]]
then
	echo 'Please provide a valid integer for the warning and critical values'
	exit 2
fi

CHECK_SERVICE=`systemctl status chronyd.service | grep Active | awk -F'[(,)]' '{print $2}'`
if [[ $CHECK_SERVICE == 'dead' ]]
then
	echo 'CRITICAL - Chrony service is not running'
	exit 1
else
	CHECK_SERVER_SYNC=`chronyc tracking | grep 'Leap status' | cut -d":" -f2 | awk '{print $1}'`
	if [[ $CHECK_SERVER_SYNC == 'Not synchronised' ]]
	then
		echo  'CRITICAL - Chrony service is not synchronised with the NTP server'
		exit 1
	else
		# Nanoseconds
		CHECK_TIME_DIFF=`chronyc tracking | grep 'System time' | cut -d":" -f2 | awk '{print $1}'`
		# Seconds (rounded)
		CHECK_TIME_DIFF_SEC=`printf "%.0f" $CHECK_TIME_DIFF`

		# Chrony does not step the system clock for corrections because this can be problematic for some time-bound processes.
		# Instead, chrony speeds up of slows down the system clock until NTP is reached, then sets the clock in normal speed.
		CLOCK_SPEED=`chronyc tracking | grep 'System time' | cut -d":" -f2 | awk '{print $3}'`

		if [[ $CHECK_TIME_DIFF_SEC -gt $1  ]] && [[ $CHECK_TIME_DIFF_SEC -lt $2  ]]
		then
			echo "WARNING - time is $CHECK_TIME_DIFF_SEC $CLOCK_SPEED to NTP Time"
			exit 1
		elif [[ $CHECK_TIME_DIFF_SEC -ge $2 ]]
		then
			echo "CRITICAL - time is $CHECK_TIME_DIFF $CLOCK_SPEED of NTP Time"
			exit 1
		else
			echo "OK - time is $CHECK_TIME_DIFF $CLOCK_SPEED of NTP Time"
			exit 0
		fi
	fi
fi